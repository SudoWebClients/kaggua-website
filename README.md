# KAGGUA Website


[![PyPI](https://img.shields.io/pypi/l/fsfe-reuse.svg)](https://www.gnu.org/licenses/gpl-3.0.html)
[![REUSE compliant](https://reuse.software/badge/reuse-compliant.svg)](https://reuse.software/)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
---


### Contents ###

* Website source code
* Version: v0.5
* [Website address](https://bitbucket.org/tutorials/markdowndemo)

### Tools ###

* Gatsby.js
* Sanity.io
* HTML, CSS, JS

### Install ###

* Clone repo to local machine.
* Run the command, npm install.
* Move into web directory and run, gatsby develop (install any other dependencies if necessary).
* Move into studio directory and run, sanity install.
* Run sanity start to check all is working on the back end.

All should be ok. 

## Contact & Sundries

* mark.webster[at]wanadoo.fr
* Version v0.5

## License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

For more information https://www.gnu.org/licenses/gpl-3.0.en.html

The program in this repository meet the requirements to be REUSE compliant,
meaning its license and copyright is expressed in such as way so that it
can be read by both humans and computers alike.

For more information, see https://reuse.software/