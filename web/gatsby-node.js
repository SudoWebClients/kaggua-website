/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 * https://css-tricks.com/how-to-make-taxonomy-pages-with-gatsby-and-sanity-io/
 */
 const path = require('path')

 exports.createPages = async({ actions, graphql }) => {
 
 const articles = await graphql( `
 query MyQuery {
    allSanityArticle {
      edges {
        node {
          slug {
            current
          }
        }
      }
    }
  }
   `
   );
 
   const theArticle = articles.data.allSanityArticle.edges.map(({ node }) => node); 
 
   theArticle.forEach( item => {
       actions.createPage({
           component: path.resolve('./src/templates/article.js'),
           path: `/article/${item.slug.current}`,
           context: {
               slug: item.slug.current
           }
       });
   })
   await createCategoryPages(graphql, actions)

  
 }

 async function createCategoryPages (graphql, actions) {
  // Get Gatsby‘s method for creating new pages
  const {createPage} = actions
  // Query Gatsby‘s GraphAPI for all the categories that come from Sanity
  // You can query this API on http://localhost:8000/___graphql
  const result = await graphql(`{
    allSanityTag {
      nodes {
        slug {
          current
        }
        id
      }
    }
  }
  `)
  // If there are any errors in the query, cancel the build and tell us
  if (result.errors) throw result.errors

  // Let‘s gracefully handle if allSanityCatgogy is null
  const categoryNodes = (result.data.allSanityTag || {}).nodes || []

  categoryNodes
    // Loop through the category nodes, but don't return anything
    .forEach((node) => {
      // Desctructure the id and slug fields for each category
      const {id, slug = {}} = node
      // If there isn't a slug, we want to do nothing
      if (!slug) return

      // Make the URL with the current slug
      const path = `/categories/${slug.current}`

      // Create the page using the URL path and the template file, and pass down the id
      // that we can use to query for the right category in the template file
      createPage({
        path,
        component: require.resolve('./src/templates/category.js'),
        context: {id}
      })
    })
}

// This is used for tag search of articles
exports.createResolvers = ({createResolvers}) => {
  const resolvers = {
    SanityTag: {
      posts: {
        type: ['SanityArticle'],
        resolve (source, args, context, info) {
          return context.nodeModel.runQuery({
            type: 'SanityArticle',
            query: {
              filter: {
                tag: {
                  elemMatch: {
                    _id: {
                      eq: source._id
                    }
                  }
                }
              }
            }
          })
        }
      }
    }
  }
  createResolvers(resolvers)
}
 
 