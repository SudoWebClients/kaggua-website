/**
 * Sanity Client Configuration
 */

const sanityClient = require('@sanity/client')

const Client = sanityClient({
  projectId: 'yiph2ik9',
  dataset: 'development',
  apiVersion: '2022-03-27', // use current UTC date - see "specifying API version"!
  //token: 'sanity-auth-token', // or leave blank for unauthenticated usage
  useCdn: true, // `false` if you want to ensure fresh data
})

export default Client


