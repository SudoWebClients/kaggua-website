import React from "react";
import { useState } from "react";
import { Link } from "gatsby";
import Home from "@material-ui/icons/Home";
import BusinessIcon from "@material-ui/icons/Business";
import IdentityIcon from "@material-ui/icons/Face";
import ExpertiseIcon from "@material-ui/icons/List";
import EmailIcon from "@material-ui/icons/Subscriptions";
import ContactIcon from "@material-ui/icons/EmailOutlined";
import "../components/nav.css"

const Header = () => {

  const [showSideMenu, setShowSideMenu] = useState(false);

  
  return (
    
    <button className="nav-bar-button" onMouseOver={()=>setShowSideMenu(true)} 
                                            onMouseLeave={()=>setShowSideMenu(false)}>

        <div className="nav-bar-icons">
            <Home  className="icon-home" fontSize="large"  />
            <IdentityIcon className="icon-home" fontSize="large"  />
            <BusinessIcon  className="icon-home" fontSize="large" />
            <ExpertiseIcon className="icon-home" fontSize="large"  />
            <EmailIcon className="icon-home" fontSize="large"  />
            <ContactIcon className="icon-home" fontSize="large"  />

    </div>                                            
    <div className= {`nav-div ${showSideMenu ? "show-side-menu" : "hide-nav"}`} >
      <nav className= "side-nav-bar" >
        <ul className="side-nav-links">  
            <li className="side-nav-elms">  
                <Link to="/" >               
                    <div className="side-nav-link">
                    Accueil
                    </div>
                </Link>
            </li>

            <li className="side-nav-elms">
            <Link to="/identity" >
                <div className="side-nav-link">
                    Identité
                </div>
            </Link>
            </li>    
            <li className="side-nav-elms">
            <Link to="/services" >
            <div className="side-nav-link">
                Approche
                </div>
            </Link>
            </li>


            <li className="side-nav-elms">
            <Link to="/modality" >
            <div className="side-nav-link">
                Expertises
                </div>
            </Link>
            </li>
            <li className="side-nav-elms">
            <Link to="/articles" >
            <div className="side-nav-link">
                Publications
            </div>
            </Link>
            </li>
            <li className="side-nav-elms">
            <a  href="mailto:contact@kaggua.com">
            <div className="side-nav-link">
                Contact
            </div>
            </a>
            
            </li>
        </ul>
    </nav>
    </div>
    </button>

  );
};

export default Header;
