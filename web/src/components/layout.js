import React from "react";
import MainLogo from "../images/logo_Kaggua_blue_web.svg";
import Header from "./header";
import MobileNav from "./mobileNavigation"
import Footer from "./footer";

const Layout = (props) => {
  return (
    <div>
      <Header />
      <MobileNav />
      <main>
        <MainLogo className="navLogo" />
        {props.children}
        <Footer />
      </main>
    </div>
  );
};

export default Layout;
