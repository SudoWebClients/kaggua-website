import React from 'react'
import {useStaticQuery, Link, graphql } from 'gatsby'
import * as FooterStyles from '../components/footer.module.css'
import LinkedInIcon from "@material-ui/icons/LinkedIn";
import TwitterIcon from "@material-ui/icons/Twitter";

const Footer = () => {

  const data = useStaticQuery(graphql`
  query MyQuery {
  sanityFooter {
    address
    legal
    href
    hrefTwitter
  }
}
`)

const socialHref = data.sanityFooter.href;
const twitterHref = data.sanityFooter.hrefTwitter;

    return(
      
        <footer className={ FooterStyles.footer }>
          <div className={ FooterStyles.content}>
                  
            <div className={FooterStyles.address}>
                <p>Informations</p>
                {/*
                <p className={FooterStyles.address}>{data.sanityFooter.address}</p>
                */}              
                <p>Directrice : Mme. Clothilde Messin</p>
                <p>Tel : 06 31 30 02 75</p>
                <p><a href="mailto:contact@kaggua.com">contact@kaggua.com</a></p>
            </div>
                
            <div className={FooterStyles.legal}>
                <p>Mentions légales</p>  
                <p className={FooterStyles.legal}> {data.sanityFooter.legal} </p>
                <p className={FooterStyles.linksLegal}>
                  <Link to="/legal" style={{
                    color: '#e8412d'
                    }}
                      >Lisez nos conditions générales </Link>
                </p>
            </div>
            <div className={FooterStyles.social}>
              <p>
                <a href={socialHref}> 
                  <LinkedInIcon className={FooterStyles.iconFooter} fontSize="large"  />
                </a>

                <a href={twitterHref}> 
                  <TwitterIcon className={FooterStyles.iconFooter} fontSize="large"  />
                </a>
              </p>
              <a  href="mailto:contact@kaggua.com">
                <button className={FooterStyles.footerButton}> join us</button>
              </a>
            </div>
          </div>
        </footer>
    )
}

export default Footer