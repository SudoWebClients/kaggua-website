import React from "react";
import { useState } from "react";
import { Link } from "gatsby";
import "../components/mobileNav.css"


const MobileNavigation = () => {

    const [showMenu, setShowMenu] = useState(false);
  
    const handleShowMenu = () => {
        setShowMenu(!showMenu)
    }

    return(
              
      <nav className={`mobile-nav-bar ${showMenu ? "show-menu" : "hide-nav"}`}>
        <ul className="nav-links">  
            <li className="nav-elms">
                <Link  to="/" >
                <div className="nav-link">
                    Accueil
                </div>
                </Link>
                
            </li>
            <li className="nav-elms">
            <Link to="/identity" >
            <div className="nav-link">
                Identité
            </div>
            </Link>
            </li>    
            <li className="nav-elms">
            <Link to="/services" >
            <div className="nav-link">
                Approche
            </div>
            </Link>
            </li>


            <li className="nav-elms">
            <Link to="/modality" >
            <div className="nav-link">
                Expertises
            </div>
            </Link>
            </li>
            <li className="nav-elms">
            <Link to="/articles" >
            <div className="nav-link">
                Publications
             </div>   
            </Link>
            <a href="mailto:contact@kaggua.com" className="nav-link-mail">
            <div className="nav-link">
                    Contact
            </div>
            </a>
            </li>
        </ul>

        <button className="nav-bar-burger" onClick={handleShowMenu}>
            <span className="burger-bar"></span>
        </button>
    </nav>

  );
}

export default MobileNavigation