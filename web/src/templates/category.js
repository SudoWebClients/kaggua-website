import React from 'react'
import {Link, graphql} from 'gatsby'

import Layout from '../components/layout'
import * as CategoriesStyles from '../components/categories.module.css'


export const query = graphql`
  query CategoryTemplateQuery($id: String!) {
    category: sanityTag(id: {eq: $id}) {
        tag
        description
        posts {
            title
            subtitle
            slug {
              current
            }
          }
    }
  }
`
const CategoryPostTemplate = props => {
  const {data = {}, errors} = props
  const {tag, description, posts} = data.category || {}

  return (
    <div className='categories-page'>
    <Layout>
      <>
  
        {!data.category && <p>No category data</p>}
        <div title={tag} description={description} />
        <section className={CategoriesStyles.mainContent}>
          {/* <h1>Category: {tag}</h1>
         <p>{description}</p>*/}

          {posts && (
            <React.Fragment>
              <h2>Publications avec la catégorie: {tag}</h2>
              <ul>
                { posts.map(post => (
                  <li key={post._id}>
                    <div className={CategoriesStyles.articleLink}>  
                        <Link to={ `/article/${post.slug.current}`}>{post.title}</Link>
                    </div>
                    <p className={CategoriesStyles.subTitle}>{post.subtitle}</p>
                  </li>))
                }
              </ul>
            </React.Fragment>)
          }
        </section>
      </>
    </Layout>
    </div>
  )
}

export default CategoryPostTemplate