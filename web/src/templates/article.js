import React from "react";
import { Link, graphql } from "gatsby";
import { GatsbyImage } from "gatsby-plugin-image";
import urlBuilder from "@sanity/image-url";
import myConfiguredSanityClient from "../components/sanityclient";
import { getImageDimensions } from "@sanity/asset-utils";
import { PortableText } from "@portabletext/react";
import Layout from "../components/layout";
import * as ArticleStyles from "../components/article.module.css";

// import our SanityClient configs for image URL builder
const builder = urlBuilder(myConfiguredSanityClient);

export const query = graphql`
  query ($slug: String!) {
    sanityArticle(slug: { current: { eq: $slug } }) {
      _createdAt(formatString: "DD/MM/YYYY")
      author {
        name
      }
      slug {
        current
      }
      tag {
        tag
        slug {
          current
        }
      }
      title
      subtitle
      _rawText
      image {
        asset {
          gatsbyImageData(layout: FULL_WIDTH, width: 400)
        }
      }
    }
  }
`;

// Barebones lazy-loaded image component
const SampleImageComponent = ({ value, isInline }) => {
  const { width, height } = getImageDimensions(value);
  return (
    <img
      src={builder // pass the builder here
        .image(value)
        .width(isInline ? 100 : 800)
        .fit("max")
        .auto("format")
        .url()}
      alt={value.alt || "no image"}
      loading="lazy"
      style={{
        // Display alongside text if image appears inside a block text span
        display: isInline ? "inline-block" : "block",
        // Avoid jumping around with aspect-ratio CSS property
        aspectRatio: width / height,
      }}
    />
  );
};

const components = {
  types: {
    image: SampleImageComponent,
    // Any other custom types you have in your content
    // Examples: mapLocation, contactForm, code, featuredProjects, latestNews, etc.
  },
};


  const handleData = (event) => {
    console.log(event)
  
}

const Article = ({ data }) => {
  //console.log(data);
  const title = data.sanityArticle.title;
  const sub = data.sanityArticle.subtitle;
  const authors = data.sanityArticle.author;
  const tags = data.sanityArticle.tag;
  
  const date = data.sanityArticle._createdAt;
  const articleMainImage = data.sanityArticle.image.asset.gatsbyImageData;
  
  
  return (
    <div className="news-page">
      <Layout>
        <div className={ArticleStyles.newsMainContent}>
          <section>
            <h1>Publications</h1>
            {/*<div className="gradient-green-blue"> </div>*/}
            <div className={ArticleStyles.info}>
              <p>Date : {date}</p>
              <p className={ArticleStyles.authors}>
                Auteurs:&nbsp;
                {authors.map((item, index) => (
                  <p key={index}>{item.name} {(item.length - 1 !== index) && ' '}</p>
                ))}
              </p>
              <br></br>

              <p className={ArticleStyles.metaTag}>
                Catégories:&nbsp;
                {tags.map((item, index) => (
                  // I need a means for getting the tag.slug.current on click !!!!
                  <Link onClick={handleData}to={`/categories/${item.slug.current}`}>
                    <p key={index}>{item.tag}{(item.length - 1 !== index) && ', '} </p>
                  </Link>
                ))}
              </p>
            </div>
            <div className={ArticleStyles.sideImage}>
              <GatsbyImage
                image={articleMainImage}
                alt="please include an alt"
              />
            </div>
          </section>

          <section className={ArticleStyles.articleMainContent}>
            <h1>{title}</h1>
            <h2 className={ArticleStyles.subTitle}>{sub}</h2>
            {
              <PortableText
                value={data.sanityArticle._rawText}
                components={components}
              />
            }
          </section>
        </div>
      </Layout>
    </div>
  );
};

export default Article;
