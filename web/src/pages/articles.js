import React from 'react'
import {Link, graphql } from 'gatsby'
import { GatsbyImage } from "gatsby-plugin-image"
//import {PortableText} from '@portabletext/react'
import Layout from '../components/layout'
import * as ArticlesStyles from "../components/articles.module.css"

export const query = graphql `
    {
        allSanityArticle(sort: {fields: _createdAt, order: DESC}) {
            edges {
              node {
                title
                slug {
                  current
                }
                author {
                  name
                }
                tag{
                  tag
                  slug {
                    current
                  }
                }
                _rawText
                image {
                  asset {
                    gatsbyImageData
                  }
                }
              }
            }
          } 
    }
`

const Articles = ( { data }) => {
    const articlesData = data.allSanityArticle.edges;
    return(
    <div className="news-page">
        <Layout>
        <div className={ArticlesStyles.articlesMainContent}>
        <h1 className="section-title">Publications</h1> 
          {/*<div className="gradient-green-blue"> </div> */}

            <ol className={ArticlesStyles.imagePanels}>
                {
                    articlesData.map(( { node }) => {
                        //const title = node.title;
                        return(
                            <li key={node.slug.current}>
  
                                <Link to={ `/article/${node.slug.current}`}>
                                    <div className={ArticlesStyles.imagePanelItem}>

                                      <div className={ArticlesStyles.imagePanelImg}>
                                          <GatsbyImage image={node.image.asset.gatsbyImageData} alt="please include an alt"  />
                                      </div>
                                    </div>
                               
                                </Link>
                                <div className={ArticlesStyles.newsInfo}> 
                                    <h2> {node.title} </h2>
                                    <p className={ArticlesStyles.authors}> Auteurs : 
                                    {node.author.map((item, index) => (
                                              <p key={index}>{item.name} {(item.length - 1 !== index) && ' '} </p>
                                          ))}
                                      </p>
                                  </div>

                                <p className={ArticlesStyles.metaTag}>Catégories : 
                                      {node.tag.map((item, index) => (
                        
                                  <Link to={`/categories/${item.slug.current}`}>
                                    <p key={index}>{item.tag}{(item.length - 1 !== index) && ', '} </p>
                                  </Link>
                                  ))}
                                </p>
                                   
                            </li>
                        )
                    })
                }
            </ol>
            </div>
        </Layout>
        </div>
      )
    }

export default Articles