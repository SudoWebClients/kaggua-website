import * as React from "react";
import { Link, graphql } from "gatsby";
import { GatsbyImage } from "gatsby-plugin-image";
import Layout from "../components/layout";
import * as IndexStyles from "../components/index.module.css";

export const query = graphql`
  {
    sanityHome {
      slogan
      news {
        title
        subtitle
        image {
          asset {
            gatsbyImageData
          }
        }
        slug {
          current
        }
      }
    }
  }
`;
const IndexPage = ({ data }) => {
  //console.log( data )
  const headline = data.sanityHome.slogan;
  const newsArticles = data.sanityHome.news;

  return (
    <div className="home-page">
      <Layout>
        <div className="home-main-content">
          <h1 className="slogan">{headline}</h1>
          <h2>Nos publications récentes</h2>
          <ol className={IndexStyles.imagePanels}>
            {
            newsArticles.map( item  => {
             
              return (
                <li key={item.slug.current}>
                  <Link to={`/article/${item.slug.current}`}>
                    <div className={IndexStyles.imagePanelItem}>
                      <div className={IndexStyles.imagePanelImg}>
                        <GatsbyImage
                          image={item.image.asset.gatsbyImageData}
                          alt="please include an alt"
                        />
                      </div>
                      
                    </div>
                    <div className={IndexStyles.infos}>
                      <h2 className={IndexStyles.title}> {item.title} </h2>
                      <p className={IndexStyles.subtitle}> {item.subtitle} </p>
                    </div>
                  </Link>
                </li>
              );
            })
            }
          </ol>
        </div>
      </Layout>
    </div>
  );
};

export default IndexPage;
