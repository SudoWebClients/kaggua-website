import * as React from "react"
import { Link, graphql } from 'gatsby'
//import { GatsbyImage } from "gatsby-plugin-image"
import { PortableText } from "@portabletext/react";
import urlBuilder from '@sanity/image-url'
import myConfiguredSanityClient from '../components/sanityclient'
import {getImageDimensions} from '@sanity/asset-utils'
import Layout from '../components/layout'
import * as ServicesStyles from "../components/services.module.css"


// import our SanityClient configs for image URL builder
const builder = urlBuilder(myConfiguredSanityClient)

export const query = graphql `
  {
    sanityServices {
      title
      subTitle
      _rawText
      slogan
      linkText
    }
  }
`
// Barebones lazy-loaded image component
const SampleImageComponent = ({value, isInline}) => {
  const {width, height} = getImageDimensions(value)
  return (
    <img
      src={builder // pass the builder here
        .image(value)
        .width(isInline ? 100 : 800)
        .fit('max')
        .auto('format')
        .url()}
      alt={value.alt || 'no image'}
      loading="lazy"
      style={{
        // Display alongside text if image appears inside a block text span
        display: isInline ? 'inline-block' : 'block',

        // Avoid jumping around with aspect-ratio CSS property
        aspectRatio: width / height,
      }}
    />
  )
}

const components = {
  types: {
    image: SampleImageComponent,
    // Any other custom types you have in your content
    // Examples: mapLocation, contactForm, code, featuredProjects, latestNews, etc.
  },
}

const Services = ({ data }) => {
  const heading = data.sanityServices.title;
  const subheading = data.sanityServices.subTitle;
  const slogan = data.sanityServices.slogan;
  const linkText = data.sanityServices.linkText;
  
  return (
    <div className="services-page">
    <Layout>
    <>
    {/*
        <div className={ServicesStyles.servicesMainContent}>
        <section className={ServicesStyles.sideSection}>
          <h1>Notre approche</h1> 
            <div className="gradient-green"> </div> 
       </section>

  */}  
        <section className={ServicesStyles.servicesMainText}>
          <h1> {heading} </h1>
          <h2 className={ServicesStyles.subTitle}>{subheading}</h2>
              {<PortableText value={data.sanityServices._rawText} components={components} />}
        

        <section className={ServicesStyles.bottomPageInfos}>
            <p> {slogan}</p>
            <Link to="/modality" >
                <button className='page-links-button'> {linkText}</button>
            </Link>
                
          </section> 
          </section>  
      </>
    </Layout>
    </div>
  );
};

export default Services
