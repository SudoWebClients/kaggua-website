import * as React from "react";
import { Link, graphql } from "gatsby";
//import { GatsbyImage } from "gatsby-plugin-image";
import { PortableText } from "@portabletext/react";
import urlBuilder from "@sanity/image-url";
import myConfiguredSanityClient from "../components/sanityclient";
import { getImageDimensions } from "@sanity/asset-utils";
import Layout from "../components/layout";
import * as ModalitiesStyles from "../components/modality.module.css";

// import our SanityClient configs for image URL builder
const builder = urlBuilder(myConfiguredSanityClient);

export const query = graphql`
  {
    sanityModalities {
      title
      subTitle
      _rawText
      slogan
      linkText
    }
  }
`;

// Barebones lazy-loaded image component
const SampleImageComponent = ({ value, isInline }) => {
  const { width, height } = getImageDimensions(value);
  return (
    <img
      src={builder // pass the builder here
        .image(value)
        .width(isInline ? 100 : 800)
        .fit("max")
        .auto("format")
        .url()}
      alt={value.alt || "no image"}
      loading="lazy"
      style={{
        // Display alongside text if image appears inside a block text span
        display: isInline ? "inline-block" : "block",

        // Avoid jumping around with aspect-ratio CSS property
        aspectRatio: width / height,
      }}
    />
  );
};

const components = {
  types: {
    image: SampleImageComponent,
    // Any other custom types you have in your content
    // Examples: mapLocation, contactForm, code, featuredProjects, latestNews, etc.
  },
};

const Modalities = ({ data }) => {
  const content = data.sanityModalities;
  const slogan = data.sanityModalities.slogan;
  const linkText = data.sanityModalities.linkText;
  
  return (
    <div className="modality-page">
      <Layout>
        <>
      {/*
        <div className={ModalitiesStyles.modalitiesMainContent}>
          <section className={ModalitiesStyles.sideSection}>
            <h1 className="section-title">Nos expertises</h1>
            <div className="gradient-blue"> </div>
          </section>
      */}
          <section className={ModalitiesStyles.modalitiesMainText}>
            <h1> {content.title} </h1>
            <h2 className={ModalitiesStyles.subTitle}>{content.subTitle}</h2>
            {<PortableText value={content._rawText} components={components} />}

            <section className={ModalitiesStyles.bottomPageInfos}>
            <p> {slogan}</p>
            <Link to="/services" >
                <button className='page-links-button'> {linkText}</button>
            </Link>
                
          </section> 
          </section>
        </>
      </Layout>
    </div>
  );
};
export default Modalities;
