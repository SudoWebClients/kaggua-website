import * as React from "react";
import { Link, graphql } from 'gatsby'
import { GatsbyImage } from "gatsby-plugin-image"
import { PortableText } from "@portabletext/react";
import Layout from "../components/layout";

import * as IdentityStyles from "../components/identity.module.css"

export const query = graphql `
  {
    sanityIdentity {
      title
      subTitle
      _rawText
      images {
        asset {
          gatsbyImageData
        }
        _key
        caption
      }

      _rawTextTwo
      slogan
      linkText
    }
  }
`

const Presentation = ( { data }) => {
  const heading = data.sanityIdentity.title;
  const subheading = data.sanityIdentity.subTitle;
  const theImages = data.sanityIdentity.images;
  const slogan = data.sanityIdentity.slogan;
  const linkText = data.sanityIdentity.linkText;
  
  
  return (
    <div className="identity-page">
    <Layout>
      <>
      {/*
    <div className={IdentityStyles.identityMainContent}>
        <section className={IdentityStyles.sideSection}>
          <h1>Identité</h1> 
          
          <div className="gradient-red"> </div>   
          <GatsbyImage class={IdentityStyles.sideImage} image={ sideImage }
                          alt="please include an alt"/>
                          
        </section>
         */}
        <article className={IdentityStyles.identityMainContent}>
       
        <section > 
          <h1>{heading}</h1>
          <h2 className={IdentityStyles.subHeading}>{subheading}</h2>
              {<PortableText value={data.sanityIdentity._rawText} />}
        </section> 

        <section>
          <ol className={IdentityStyles.imagePanels}>
        {
              theImages.map( item => {

                return(
                  <div className={IdentityStyles.imagePanelItem}>
                    <div className={IdentityStyles.imagePanelImage}>
                      <li key={ item._key}>
                        <GatsbyImage
                          image={ item.asset.gatsbyImageData}
                          alt ="please include an alt"
                        />
                      </li>

                    </div>
                    <div className={IdentityStyles.imageCaption}>
                        <p> { item.caption }</p>
                    </div>
                  </div>
                  
                )
              })
            }
            </ol>
        </section> 

            <section > 
              <h2 className={IdentityStyles.subHeading}>{subheading}</h2>
                {<PortableText value={data.sanityIdentity._rawTextTwo} />}
            </section>

          <section className={IdentityStyles.bottomPageInfos}>
            <p> {slogan}</p>
            <Link to="/services" >
                <button className='page-links-button'> {linkText}</button>
            </Link>
                
          </section>  
        </article>     
    </>  
    </Layout>
    </div>
  );
};

export default Presentation;
