import * as React from "react";
import { useStaticQuery, graphql } from "gatsby";
import { PortableText } from "@portabletext/react";
import Layout from "../components/layout";


const LegalPage = () => {
    
    const legalData = useStaticQuery(graphql`
    query MyLegal {
    sanityFooter {
        _rawText
    }
  }
  `)

    console.log(legalData)
    return(
        <Layout>

            <div className="legal-page">
            <PortableText
                value={legalData.sanityFooter._rawText}
              />
            </div>
        </Layout>
    )
}


export default LegalPage