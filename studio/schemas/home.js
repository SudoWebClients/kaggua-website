/**
 * Page d'accueil
 */

export default{
    name: 'home',
    title: 'Page d\'accueil',
    type: 'document',

    fields: [
        {
            name: 'slogan',
            title: 'Slogan du moment',
            type: 'string'
        },
        {
            name: 'customimage',
            title: 'Home Page Image',
            type: 'image',
            options: {
                hotspot: true
              }
        },
        {
            name: 'imageURL',
            title: 'Add URL for Image link',
            type: 'url'
        },
        {
            name: 'news',
            title: 'Derniers articles',
            type: 'array',
            of: [
                {
                    type: 'reference',
                    to: [{type:'article'}]
                }
            ]
        },
        {
            name: 'sloganTwo',
            title: 'Slogan',
            type: 'string'
        },
        {
            name: 'href',
            title: 'Lien vers nouvelle page',
            type: 'url'
          },
          {
            name: 'linkText',
            title: 'Texte lien',
            type: 'string'
        }  
    ]
}