/**
 *  Defines an author that can attributed to any blog content
 */
export default{
    name: 'author',
    title: 'Auteur',
    type: 'document',

    fields: [
        {
            name: 'name',
            title: 'Nom',
            type: 'string'
        },
        {
            name: 'bio',
            title: 'Biographie court',
            type: 'array',
            of: [
                {
                    title: 'Block',
                    type: 'block',
                    styles: [{ title: 'Normal', value: 'normal' }]
                }
            ]
        },
        {
            name: 'image',
            title: 'Image',
            type: 'image',
            options: {
                hotspot: true
            }
        },
        {
            name: 'slug',
            title: 'Slug',
            type: 'slug',
            options:{
                source: 'name',
                maxLength: 30
            }
        }
    ]



}