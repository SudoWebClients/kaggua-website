/**
 * Defines tags for content
 */


export default {
    name: 'tag',
    title: 'Tag',
    type: 'document',

    fields: [
        {
            name: 'tag',
            title: 'Tag',
            type: 'string'
        },

        {
            name: 'description',
            title: 'Descritpion du métadonné',
            type: 'text'
        },
        {
            name: 'slug',
            type: 'slug',
            title: 'Slug',
            options: {
              // add a button to generate slug from the title field
              source: 'tag'
            }
          },
    ]
}