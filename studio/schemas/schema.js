// First, we must import the schema creator
import createSchema from 'part:@sanity/base/schema-creator'

// Then import schema types from any plugins that might expose them
import schemaTypes from 'all:part:@sanity/base/schema-type'
import settings from './siteSettings'
import footer from './footer'
import home from './home'
import identity from './identity'
import services from './services'
import modalities from './modalities'
import article from './article'
import author from './author'
import tag from './tag'
import images from './images'

// Then we give our schema to the builder and provide the result to Sanity
export default createSchema({
  // We name our schema
  name: 'default',
  // Then proceed to concatenate our document type
  // to the ones provided by any plugins that are installed
  types: schemaTypes.concat([
    /* Your types here! */
    settings,
    footer,
    home,
    identity,
    services,
    modalities,
    article,
    author,
    tag,
    images
  ]),
})
