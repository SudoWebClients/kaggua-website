/**
 *  Main Site settings
 */


 export default {
    name: 'footer',
    title: 'Footer',
    type: 'document',

    fields: [
      {
        name: 'address',
        title: 'Contacte',
        type: 'text'
      },
      {
        name: 'legal',
        title: 'Mentions Légales',
        type: 'text'
      },
    {
      name: 'href',
      title: 'LinkedIn URL',
      type: 'url'
    },

  {
    name: 'hrefTwitter',
    title: 'Twitter URL',
    type: 'url'
  },
    {
      name: 'text',
      title: 'Conditions générales',
      type: 'array',
      of: [
          {
              type: 'block'
          }
      ]
  },
  ]
  
    
  }