/**
 * Main articles / blog
 */


export default{
    name: 'article',
    title: 'Articles / Actus',
    type: 'document',

    fields: [

        {
            name: 'title',
            title: 'Titre',
            type: 'string'
        },
        {
            name: 'subtitle',
            title: 'Sous-Titre',
            type: 'string'
        },        
        {
            name: 'author',
            title: 'Auteur',
            type: 'array',
            of: [
                {
                    type: 'reference',
                    to: [{type:'author'}]
                }
            ]
        },
        {
            name: 'image',
            title: 'Image Principale de l\'article',
            type: 'image',
            options: {
                hotspot: true
              }
        },        
        {
            name: 'text',
            title: 'Texte',
            type: 'array',
            of: [
                {
                    type: 'block'
                },
                {
                    type: 'image'
                }
            ]
        },
        {
            name: 'tag',
            title: 'Tag',
            type: 'array',
            of: [
                {
                    type: 'reference',
                    to: [{type: 'tag'}]
                }
            ]
        },
        {
            name: 'slug',
            title: 'Slug',
            type: 'slug',
            options: {
                source: 'title',
                maxLength: 200, // will be ignored if slugify is set
                slugify: input => input
                                     .toLowerCase()
                                     .replace(/\s+/g, '-')
                                     .slice(0, 200)
            }
        }
    ]
}