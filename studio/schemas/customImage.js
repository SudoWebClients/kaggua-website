/**
 * Custom Image (for home page eventually)
 */

 export default {  
    name: 'customimage',
    title: 'Image principale de la page d\'accueil',
    type: 'image',
    options: {
        hotspot: true
      },
      fields: [
        {
          title: 'Ajouter URL pour l\'image',
          name: 'href',
          type: 'url'
        }
      ]
}