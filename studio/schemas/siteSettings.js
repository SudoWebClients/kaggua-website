/**
 *  Main Site settings
 */


export default {
    name: 'siteSettings',
    title: 'Site Settings',
    type: 'document',
    //__experimental_actions: [/*'create',*/ 'update', /*'delete',*/ 'publish'], 

    fields: [
      {
        name: 'title',
        title: 'Title pricipale du site',
        type: 'string'
      },
      {
        name: 'description',
        title: 'Description du contenu du site',
        type: 'text'
      }
    ]
  }