/**
 *  Identity Schema
 */


export default {

    name: 'identity',
    title: 'Identité',
    type: 'document',

    fields: [
        {
            name: 'title',
            title: 'Titre',
            type: 'string'
        },  
        {
            name: 'subTitle',
            title: 'Sous-Titre',
            type: 'string'
        },
        {
            name: 'text',
            title: 'Première Texte',
            type: 'array',
            of: [
                {
                    type: 'block'
                },
                {
                    type: 'image'
                }
            ]
        }, 
        {
            name: 'images',
            title: 'Images',
            type: 'array',
            of: [
                {
                    type: 'images',
                }
            ]
        },        
        {
            name: 'textTwo',
            title: 'Deuxième Texte',
            type: 'array',
            of: [
                {
                    type: 'block'
                },
                {
                    type: 'image'
                }
            ]
        },
        {
            name: 'slogan',
            title: 'Slogan',
            type: 'string'
        },
        {
            name: 'href',
            title: 'Lien vers nouvelle page',
            type: 'url'
          },
          {
            name: 'linkText',
            title: 'Texte lien',
            type: 'string'
        }          
    ]
}