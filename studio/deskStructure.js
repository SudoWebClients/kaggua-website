// /deskStructure.js
import S from '@sanity/desk-tool/structure-builder'

export default () =>
  S.list()
    .title('Contenu')
    .items([

      S.listItem()
        .title('Configuration')
        .child(
          S.list()
            // Sets a title for our new list
            .title('Configurations Globales')
            // Add items to the array
            // Each will pull one of our new singletons
            .items([
              S.listItem()
                .title('Metadata')
                .child(
                  S.document()
                  .title('Données globales')
                    .schemaType('siteSettings')
                    .documentId('siteSettings')
                ),
                S.listItem()
                .title('Identité')
                .child(
                  S.document()
                    .schemaType('identity')
                    .documentId('identity')
                ),
                S.listItem()
                .title('Approche')
                .child(
                  S.document()
                    .schemaType('services')
                    .documentId('services')
                ),
                S.listItem()
                .title('Expertises')
                .child(
                  S.document()
                    .schemaType('modalities')
                    .documentId('modalities')
                ),
              S.listItem()
                .title('Footer')
                .child(
                  S.document()
                  .title('Données de bas de page')
                    .schemaType('footer')
                    .documentId('footer')
                ),
            ]),
        ),
        S.listItem()
        .title('Page d\'Accueil')
        .child(
          S.document()
            .title('Contenu de la home page')
            .schemaType('home')
            .documentId('home')
        ),
      // We also need to remove the new singletons from the main list
      ...S.documentTypeListItems().filter(listItem => !['siteSettings', 'footer', 'identity', 'services', 'modalities', 'home'].includes(listItem.getId()))
    ])