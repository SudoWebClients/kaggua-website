import React from 'react'
import styles from './logo.css'

const myLogo = () => (
    <img className={styles.myLogo} src="/static/logo.png" alt="Kaggua Logo" />
);

export default myLogo;